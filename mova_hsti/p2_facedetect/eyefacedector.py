import argparse
import cv2
import numpy as np


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("-v", "--video", required=True, help="path to video file to track")
    ap.add_argument("-o", "--output", required=False, help="output path with detected faces and eyes")
    args = vars(ap.parse_args())
    print 'Input: '+args['video']
    print 'Output: '+args['output']
    process_video(args['video'], args['output'])


def process_video(input,output):
    cap = cv2.VideoCapture(0)

    w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    out = create_writer(output, (w, h))
    i = 0;
    while cap.isOpened():
        ret,frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # cv2.imshow('source', gray)
        detection = process_frame(gray, frame)
        ret = out.write(detection)
        if cv2.waitKey(20) & 0xFF == ord('q'):
            break
    cap.release()
    out.release()
    cv2.destroyAllWindows()
    print 'End of video file'


def create_writer(output, frame_size):
    fourccc = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
    return cv2.VideoWriter(output, fourccc, 15, frame_size, isColor=True)


def process_frame(frame, color):
    eyedector = 'haarcascade_eye.xml'
    facedetector = 'haarcascade_frontalface_default.xml'
    eyes = detect(frame, eyedector)
    face = detect(frame, facedetector)

    #color = cv2.cvtColor(frame,cv2.COLOR_GRAY2BGR)
    for (ex, ey, ew, eh) in eyes:
        cv2.rectangle(color, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 2)

    for (ex, ey, ew, eh) in face:
        cv2.rectangle(color, (ex, ey), (ex+ew, ey+eh), (0, 0, 255), 2)

    cv2.imshow('detection', color)
    return color


def detect(frame,detector):
    face_cascade = cv2.CascadeClassifier(detector)
    return face_cascade.detectMultiScale(frame, scaleFactor= 1.1, minNeighbors=5, minSize=(30,30),
                                         flags=cv2.CASCADE_SCALE_IMAGE)


if __name__ == "__main__":
    main()
