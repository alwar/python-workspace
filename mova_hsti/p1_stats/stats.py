# -*- encoding: utf-8 -*-
import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt
import sys

__author__ = 'alwar'


def field(s):
    """
Funcion para convertir los no numeros a NaN
:param s: integer
:return: integer|nan
    """
    try:
        value = float(s)
    except ValueError:
        print "No data " + s
        value = float('Nan')
    return value


def compute_area(dt, gr, mult, title='Area'):
    area_diff = np.absolute(np.subtract(gr, dt))

    area_valid, errors = clean_data(area_diff)

    b = np.count_nonzero(area_valid < 50)
    c = np.count_nonzero(area_valid < 100)
    d = np.count_nonzero(area_valid < 150)
    e = np.count_nonzero(area_valid < 200)
    f = np.count_nonzero(area_valid < 250)
    g = np.count_nonzero(area_valid >= 250)
    data_for_graph = [errors, b, c-b, d-c, e-d, f-e, g]

    xlabels = ('err', '[0-50)', '[50-100)', '[100-150)', '[150-200)', '[200-250)', '>250')
    color = ('k', 'r', 'r', 'r', 'r', 'r', 'r')
    plot_data(data_for_graph, np.arange(7), mult, title, xlabels, 'Squared feet error',
              'Percentage of blueprints', color)

    return  data_for_graph


def plot_data(raw_data, ind, mult, title, xticks, xtitle, ylabel, _colors='r'):
    width=1
    data = np.divide(np.multiply(raw_data, 100), mult)
    plt.ylim([0,100])
    plt.bar(ind, data, width, color=_colors)
    plt.ylabel(ylabel)
    plt.xlabel(xtitle)
    plt.title(title)
    plt.xticks(ind + width / 2., xticks)


def clean_data(data):
    masked_items = ma.masked_array(data, np.isnan(data))
    clead_data = masked_items[~masked_items.mask]
    items_w_errs = data.shape[0]
    items_wo_err = clead_data.shape[0]
    errors = items_w_errs - items_wo_err
    return clead_data, errors


def compute_complexity(dt, gr, mult, title='Complexity'):
    compx_diff = np.absolute(np.subtract(gr, dt))

    compx_valid, errors = clean_data(compx_diff)

    complex_graph = [
        errors,
        np.count_nonzero(compx_valid == 0),
        np.count_nonzero(compx_valid == 1),
        np.count_nonzero(compx_valid == 2),
        np.count_nonzero(compx_valid == 3),
        np.count_nonzero(compx_valid >= 4),
    ]

    xlabels = ('err', '0', '1', '2', '3', '>4')
    color = ('k', 'r', 'r', 'r', 'r', 'r')
    plot_data(complex_graph, np.arange(6), mult, title, xlabels, 'Inches per foot',
              'Percentage of blueprints',color)

    return complex_graph


def computestats(groundtruth_path, detection_path, output_path):
    print "Control: " + groundtruth_path
    print "Muestra: " + detection_path
    print "Saving image files to " + output_path

    converter = {1: field, 2: field, 3: field}

    gr = np.loadtxt(groundtruth_path, None, "", ",", converter, 1)
    dt = np.loadtxt(detection_path, None, "", ",", converter, 1)

    g_shape = gr.shape
    d_shape = dt.shape
    if g_shape!=d_shape:
        print "Error! tamaños distintos: " + str(d_shape) + " vs " + str(g_shape)
        return
    else:
        print "Data dimensions: " + str(d_shape)

    dimensions = d_shape[0]

    plt.figure(1)
    area2d_graph_gata = compute_area(dt[:, [1]], gr[:, [1]], dimensions, "Area2D")
    print "Area 2D data: " + str(area2d_graph_gata)
    plt.savefig(output_path + '/area2d.png', format='png', facecolor='#cccccc')

    plt.figure(2)
    area3d_graph_gata = compute_area(dt[:, [2]], gr[:, [2]], dimensions, "Area3D")
    print "Area 3D data: " + str(area3d_graph_gata)
    plt.savefig(output_path + '/area3d.png', format='png', facecolor='#cccccc')

    plt.figure(3)
    complexity_graph_data = compute_complexity(dt[:, [3]], gr[:, [3]], dimensions)
    print "Complexity data: " + str(complexity_graph_data)
    plt.savefig(output_path + '/complexity.png', format='png', facecolor='#cccccc')

    return

#
#  Launch application
#
groundtruth = sys.argv[1]
detection = sys.argv[2]
output = sys.argv[3]
computestats(groundtruth, detection, output)