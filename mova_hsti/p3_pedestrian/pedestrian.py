import glob
import argparse
import cv2


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", required=True, help="path to dir with sequence of images (with trailing slash)")
    ap.add_argument("-o", "--output", required=False, help="output path with detected faces and eyes")
    args = vars(ap.parse_args())
    print 'Input: '+args['image']
    print 'Output: '+args['output']
    process_video(args['image'], args['output'])


def process_video(img_path, output):
    h = 640
    w = 480
    #out = create_writer(output, (w, h))
    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
    pattern = img_path + "*.png"
    for frame in glob.glob(pattern):
        if cv2.waitKey(20) & 0xFF == ord('q'):
            break
        img = cv2.imread(frame)
        pedestrian_detector(img, hog)
        cv2.imshow('pedestrian', img)
        cv2.imwrite(frame+'-detect',img)
        #out.write(img)
    #out.release()
    cv2.destroyAllWindows()
    print 'End of video file'


def create_writer(output, frame_size):
    fourccc = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
    return cv2.VideoWriter(output, fourccc, 15, frame_size, isColor=True)


def pedestrian_detector(img, hog):
    rects, weights = hog.detectMultiScale(img, winStride=(8,8), padding=(32,32), scale=1.05)
    i = 0
    for (ex, ey, ew, eh) in rects:
        if (weights[i]>0.8):
            cv2.rectangle(img, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 2)
        else:
            cv2.rectangle(img, (ex, ey), (ex+ew, ey+eh), (0, 0, 255), 2)
        i += 1
    return

if __name__ == "__main__":
    main()
