# -*- coding: utf-8
import requests
import bs4


def getItemInfo(item):
    for tag in item.children:
        if isinstance(tag, bs4.element.Tag):
            tag_class = tag.attrs.get('class')
            if tag_class[0]=='precio':
                precio = tag.contents[1].text
            elif tag_class[0]=='nombre':
                nombre = tag.contents[1].text
    return nombre,precio


search = 'http://www.pccomponentes.com/memorias_ddr3_1600.html'

selector = '.articulo-familia'

response = requests.get(search)
soup = bs4.BeautifulSoup(response.text)
items = soup.select(selector)
for item in items:
    nombre,precio = getItemInfo(item)
    print nombre + ' >>> ' + precio