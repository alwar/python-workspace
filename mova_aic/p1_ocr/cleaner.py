import glob
import cv2
import time
import numpy as np


def process(img):
    # foo bar
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, img = cv2.threshold(img,100,255,cv2.THRESH_OTSU)
    img = cv2.dilate(img, np.array([3, 3]),iterations=20)
    cv2.imshow('clean', img)
    cv2.waitKey()
    img = cv2.Sobel((255-img), cv2.CV_8U, 0, 1, 7)
    cv2.imshow('clean', img)
    cv2.waitKey()
    edges = cv2.Canny(img,50,150,apertureSize = 3)
    lines = cv2.HoughLinesP(edges, 1, np.pi/180, 10, 80, 30, 10)

    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    if lines!=None:
        for item in lines:
            print 'linea!'
            cv2.line(img, (item[0][0], item[0][1]), (item[0][2], item[0][3]), (0, 255, 0), 2)
    return img


for image in glob.glob('./cleaning/*'):
    print 'loading image '+image
    img = cv2.imread(image)
    result = process(img)
    cv2.imshow('clean', result)
    cv2.waitKey()
print 'finish'