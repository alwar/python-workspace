import cv2
import numpy as np
import argparse
import subprocess as sp
import fileinput
import glob


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("-s", "--samples", required=True, help="path to image samples")
    args = vars(ap.parse_args())
    print 'Reading images from: '+args['samples']
    read_images(args['samples'])


def read_images(path):
    for image in glob.glob(path+'*'):
        text = process_image(image)
        print text
    print 'finish'


def process_image(path):
    text = ''
    #read image
    img = cv2.imread(path)
    ocr_image = clean_image(img)
    cv2.imwrite('./tmp_ocr.jpg', ocr_image)
    # call tesseract to action
    text = sp.check_output('/usr/local/bin/tesseract ./tmp_ocr.jpg  stdout -psm 8', shell=True)
    sp.call('rm ./tmp_ocr.jpg', shell=True)
    return text


def clean_image(img):
    #apply filters to correct image
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.threshold(img,100,255,cv2.THRESH_OTSU)
    cv2.erode(img,(3,3))
    cv2.imshow('cleaning', img)
    #umbralize
    #erode
    #crop&resize
    return img


if __name__ == "__main__":
    main()
